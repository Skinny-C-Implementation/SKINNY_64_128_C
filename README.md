# Skinny-64-128

## Notes
[Reference paper](https://eprint.iacr.org/2016/660.pdf)

The plain text in this version is 64 bits long, and the tweakey is 128 bits long. The tweakey is divided into 2 64 bits part.

The Skinny lightweight cipher is composed of 5 steps + 1 Tweakey Schedule:
- Sub Cells
- Add Constants
- Add Round Tweakey
- Shift Rows
- Mix Columns

The program takes between 2 and 3 arguments. The first two are the plain text and a key. The third optional one is the mode you want to use. If you want to encrypt the plain text, use "ENC". If you want to decrypt it, use "DEC".

## Use:

To compile the program on a Unix system, open a Terminal in the directory containing this repository's content.

```bash
make clean
make all
```

Note: If some directories are missing, you may need to redo the commands one more time.

Then, to use the program, do:

```bash
./bin/Skinny_64_128 046be0baecc1f283 d57aafd67af4483428d61a94c501d2fe
```

This command will print on the output all the rounds necessary to encrypt and decrypt the plain text with the key.
You should obtain the last following line:

```bash
DEC - round 35 - after MC: S= 2c817c994654930f - TK1 = a478f344d57aafd6 - TK2 = bc840ea228d61a94
DEC - round 35 - after SR: S= 2c81c9975446f930 - TK1 = a478f344d57aafd6 - TK2 = bc840ea228d61a94
DEC - round 35 - after AK: S= d12d7cd55446f930 - TK1 = d57aafd67af44834 - TK2 = 28d61a94c501d2fe
DEC - round 35 - after AC: S= c12d7cd57446f930 - TK1 = d57aafd67af44834 - TK2 = 28d61a94c501d2fe
DEC - round 35 - after SC: S= 046be0baecc1f283 - TK1 = d57aafd67af44834 - TK2 = 28d61a94c501d2fe

--------------------------------------
FINISH
--------------------------------------
DEC - Final State - : S= 046be0baecc1f283 - TK1 = d57aafd67af44834 - TK2 = 28d61a94c501d2fe
```

You should obtain the same output by using this line:
```bash
./bin/Skinny_64_128 046be0baecc1f283 d57aafd67af4483428d61a94c501d2fe DEC
```

With:

```bash
./bin/Skinny_64_128 046be0baecc1f283 d57aafd67af4483428d61a94c501d2fe ENC
```

You should obtain:
```bash
ENC - round 35 - after SC: S= 0933ee9480662722 - TK1 = 44a374f856df7daa - TK2 = 19f82b03239b564a
ENC - round 35 - after AC: S= d933ee94a0662722 - TK1 = 44a374f856df7daa - TK2 = 19f82b03239b564a
ENC - round 35 - after AK: S= 8468b16fa0662722 - TK1 = 6a5dda7f44a374f8 - TK2 = 654d39b719f82b03
ENC - round 35 - after SR: S= 8468fb1666a07222 - TK1 = 6a5dda7f44a374f8 - TK2 = 654d39b719f82b03
ENC - round 35 - after MC: S= 90ea84689db6e2c8 - TK1 = 6a5dda7f44a374f8 - TK2 = 654d39b719f82b03

--------------------------------------
FINISH
--------------------------------------
ENC - Final State - : S= 90ea84689db6e2c8 - TK1 = 6a5dda7f44a374f8 - TK2 = 654d39b719f82b03
```

Finally, if you want to put the output in a txt file for example, just do:

```bash
./bin/Skinny_64_128 046be0baecc1f283 d57aafd67af4483428d61a94c501d2fe > file.txt
```

Note: by putting ">>" instead of ">", you will add the output at the end of the file instead of overwriting it.