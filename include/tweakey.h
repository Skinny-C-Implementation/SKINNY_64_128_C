#pragma once

void _print_tweakey(unsigned char tk[], int size);
//void _tweakey_permutation(unsigned char tk[], int size, char *mode);
//void _tweakey_lfsr(unsigned char tk[], int size, char *mode);
void print_tweakeys(unsigned char tk1[], unsigned char tk2[], int size);
void tweakey_schedule(unsigned char tk1[], unsigned char tk2[], int size, char *mode);
void tweakey_calcul(unsigned char **tk_table, unsigned char *tk1, unsigned char *tk2, int size);
