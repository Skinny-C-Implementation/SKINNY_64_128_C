#pragma once

void print_round(unsigned char table[], int round, int size, char* stage_str, char* mode);
void print_init(unsigned char table[], int size, char* mode);
void print_final(unsigned char table[], int size, char* mode);
