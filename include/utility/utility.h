#pragma once

void matrix_multiplication(unsigned char matrix_A[], unsigned char matrix_B[], unsigned char matrix_C[], int row, int col, int is_binary);
void table_permutation(unsigned char table[], int index_table[], int size);
void byte_to_bit_table(unsigned char* byte, unsigned char table[], int size);
void bit_table_to_byte(unsigned char* byte, unsigned char table[], int size);
