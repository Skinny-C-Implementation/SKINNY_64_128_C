#pragma once

void byte_to_bit_table(unsigned char* byte, unsigned char table[], int size);
void bit_table_to_byte(unsigned char* byte, unsigned char table[], int size);
unsigned char ASCII_to_hex(char c);
