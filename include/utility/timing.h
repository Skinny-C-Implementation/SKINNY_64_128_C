#ifndef TIMING_H
#define TIMING_H

unsigned long long int start_rdtsc(void);
unsigned long long int end_rdtsc(void);

#endif
