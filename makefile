SRCDIR=src
SRCUTILDIR=$(SRCDIR)/utility
SRCTESTDIR=$(SRCDIR)/tests
SRCTESTUTILDIR=$(SRCTESTDIR)/utility
LIBDIR=lib
BINDIR=bin
INCLUDEDIR=include
TESTDIR=tests
TESTUTILDIR=$(TESTDIR)/utility
CC=gcc
AR=ar
CFLAGS=-Wall -pedantic -g -std=gnu99 -I$(INCLUDEDIR) -I$(HOME)/local/include -lm
LDFLAGS=-L$(LIBDIR) -lutility
EXEC=Skinny_64_128
EXETEST=testSkinny
DOCDIR=doc
DOCCC=doxygen
CREATE=mkdir

const :
	@ if ! [ -d $(BINDIR) ]; then \
		echo "Création du dossier $(BINDIR)"; \
		$(CREATE) $(BINDIR); \
	fi
	@ if ! [ -d $(TESTDIR) ]; then \
		echo "Création du dossier $(TESTDIR)"; \
		$(CREATE) $(TESTDIR); \
	fi
	@ if ! [ -d $(TESTSDDDIR) ]; then \
		echo "Création du dossier $(TESTSDDDIR)"; \
		$(CREATE) $(TESTSDDDIR); \
	fi
	@ if ! [ -d $(LIBDIR) ]; then \
		echo "Création du dossier $(LIBDIR)"; \
		 $(CREATE) $(LIBDIR); \
	fi
	@ if ! [ -d $(DOCDIR) ]; then \
		echo "Création du dossier $(DOCDIR)"; \
		$(CREATE) $(DOCDIR); \
		echo "Création du dossier $(DOCDIR)/html"; \
		$(CREATE) $(DOCDIR)/html; \
		echo "Création du dossier $(DOCDIR)/latex"; \
		$(CREATE) $(DOCDIR)/latex; \
	fi

all : const $(BINDIR)/$(EXEC)

$(SRCDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

$(LIBDIR)/libutility.a : $(SRCUTILDIR)/table_utility.o $(SRCUTILDIR)/byte_utility.o $(SRCUTILDIR)/print_utility.o $(SRCUTILDIR)/timing.o 
	$(AR) -r $@ $^

$(BINDIR)/$(EXEC) : $(SRCDIR)/main.o $(SRCDIR)/subcells.o $(SRCDIR)/add_constants.o $(SRCDIR)/add_round_tweakey.o $(SRCDIR)/tweakey.o $(SRCDIR)/shift_rows.o $(SRCDIR)/mix_columns.o $(LIBDIR)/libutility.a 
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)

$(SRCDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

clean :
	rm -rf ./$(BINDIR)/*
	rm -rf ./$(LIBDIR)/*.a
	rm -rf ./$(SRCDIR)/*.o
	rm -rf ./$(SRCUTILDIR)/*.o
	rm -rf ./$(SRCTESTDIR)/*.o
	rm -rf ./$(SRCTESTUTILDIR)/*.o
	rm -rf ./$(TESTDIR)/*
	rm -rf ./$(DOCDIR)/*
	rm -rf ./$(SRCDIR)/*.c~
	rm -rf ./$(SRCUTILDIR)/*.c~
	rm -rf ./$(INCLUDEDIR)/*.h~
	rm -rf ./$(INCLUDEDIR)/utility/*.h~

tests : const $(TESTSDDDIR)/test_TableDeCodage $(TESTSDDDIR)/test_Statistiques $(TESTSDDDIR)/test_Octet $(TESTSDDDIR)/test_FileDePriorite $(TESTSDDDIR)/test_FichierBinaire $(TESTSDDDIR)/test_CodeBinaire $(TESTSDDDIR)/test_ArbreDeHuffman

$(TESTSDDDIR)/test_TableDeCodage : $(SRCTESTSDDDIR)/test_TableDeCodage.o $(LIBDIR)/libsdd.a
		$(CC) -o $(TESTSDDDIR)/test_TableDeCodage $(SRCTESTSDDDIR)/test_TableDeCodage.o $(LDFLAGS) $(CFLAGS) -lcunit

$(TESTSDDDIR)/test_Statistiques : $(SRCTESTSDDDIR)/test_Statistiques.o $(LIBDIR)/libsdd.a
		$(CC) -o $(TESTSDDDIR)/test_Statistiques $(SRCTESTSDDDIR)/test_Statistiques.o $(LDFLAGS) $(CFLAGS) -lcunit

$(TESTSDDDIR)/test_Octet : $(SRCTESTSDDDIR)/test_Octet.o $(LIBDIR)/libsdd.a
		$(CC) -o $(TESTSDDDIR)/test_Octet $(SRCTESTSDDDIR)/test_Octet.o $(LDFLAGS) $(CFLAGS) -lcunit

$(TESTSDDDIR)/test_FileDePriorite : $(SRCTESTSDDDIR)/test_FileDePriorite.o $(LIBDIR)/libsdd.a
		$(CC) -o $(TESTSDDDIR)/test_FileDePriorite $(SRCTESTSDDDIR)/test_FileDePriorite.o $(LDFLAGS) $(CFLAGS) -lcunit

$(TESTSDDDIR)/test_FichierBinaire : $(SRCTESTSDDDIR)/test_FichierBinaire.o $(LIBDIR)/libsdd.a
		$(CC) -o $(TESTSDDDIR)/test_FichierBinaire $(SRCTESTSDDDIR)/test_FichierBinaire.o $(LDFLAGS) $(CFLAGS) -lcunit

$(TESTSDDDIR)/test_CodeBinaire : $(SRCTESTSDDDIR)/test_CodeBinaire.o $(LIBDIR)/libsdd.a
		$(CC) -o $(TESTSDDDIR)/test_CodeBinaire $(SRCTESTSDDDIR)/test_CodeBinaire.o $(LDFLAGS) $(CFLAGS) -lcunit

$(TESTSDDDIR)/test_ArbreDeHuffman : $(SRCTESTSDDDIR)/test_ArbreDeHuffman.o $(LIBDIR)/libsdd.a
		$(CC) -o $(TESTSDDDIR)/test_ArbreDeHuffman $(SRCTESTSDDDIR)/test_ArbreDeHuffman.o $(LDFLAGS) $(CFLAGS) -lcunit

doc: $(DOCDIR)/html/index.html $(DOCDIR)/latex/refman.pdf

$(DOCDIR)/html/index.html:
	@doxygen 2> /dev/null

$(DOCDIR)/latex/refman.pdf: $(DOCDIR)/latex/refman.tex
	@cd $(DOCDIR)/latex/; make > /dev/null; cd ../..

$(DOCDIR)/latex/refman.tex:
	@doxygen 2> /dev/null
