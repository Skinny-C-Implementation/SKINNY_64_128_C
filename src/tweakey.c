#include "tweakey.h"
#include "utility/byte_utility.h"
#include "utility/table_utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int NIBBLE_SIZE = 4;

void _print_tweakey(unsigned char tk[], int size){
  for(int i=0; i<size; i++){
    printf("%x", tk[i]);
  }
}

void _tweakey_permutation(unsigned char tk[], int size, char *mode){
  int permutation_table[16] = {9, 15, 8, 13, 10, 14, 12, 11, 0, 1, 2, 3, 4, 5, 6, 7};

  if(strcmp(mode, "ENC") == 0){
    table_permutation(tk, permutation_table, size, 0);
  }
  else if(strcmp(mode, "DEC") == 0){
    table_permutation(tk, permutation_table, size, 1);
  }
}

void _lfsr(unsigned char* nibble, char *mode){
  unsigned char *tmp = (unsigned char*) malloc(NIBBLE_SIZE*sizeof(unsigned char));
  byte_to_bit_table(nibble, tmp, NIBBLE_SIZE);

  if(strcmp(mode, "ENC") == 0){
    if((tmp[0]^tmp[1]) == 0){
      *nibble = (*nibble << 1) & 0xf;
    }
    else{
      *nibble = ((*nibble << 1 | 1)) & 0xf;
    }
  }
  else if(strcmp(mode, "DEC") == 0){
    if((tmp[0]^tmp[3]) == 0){
      *nibble = (*nibble >> 1) & 0xf;
    }
    else{
      *nibble = ((*nibble >> 1 | 8)) & 0xf;
    }
  }
  
  free(tmp);
}

void _tweakey_lfsr(unsigned char tk[], int size, char *mode){
  for(int i=0;i<size/2;i++){
    _lfsr(&tk[i], mode);
  }
}

void print_tweakeys(unsigned char tk1[], unsigned char tk2[], int size){
  unsigned char** tmp = (unsigned char**)malloc(2*sizeof(unsigned char*));
  tmp[0] = tk1;
  tmp[1] = tk2;

  for(int i=0; i<2; i++){
    printf(" - TK%d = ", i+1);
    _print_tweakey(tmp[i], size);
  }
  printf("\n");
  free(tmp);
}

void tweakey_schedule(unsigned char tk1[], unsigned char tk2[], int size, char *mode){

  unsigned char** tmp = (unsigned char**)malloc(2*sizeof(unsigned char*));
  tmp[0] = tk1;
  tmp[1] = tk2;

  if(strcmp(mode, "DEC") == 0){
    _tweakey_lfsr(tk2, size, mode);
  }
  for(int i=0; i<2; i++){
    _tweakey_permutation(tmp[i], size, mode);
  }
  if(strcmp(mode, "ENC") == 0){
    _tweakey_lfsr(tk2, size, mode);
  }
  
  free(tmp);
}

void tweakey_calcul(unsigned char **tk_table, unsigned char *tk1, unsigned char *tk2, int size){
  for (int j=0; j<16; j++){
    memmove(tk_table[0], tk1, size*sizeof(unsigned char));
    memmove(tk_table[37], tk2, size*sizeof(unsigned char));
  }
  for (int i=0; i<36; i++){
    tweakey_schedule(tk1, tk2, size, "ENC");
    for (int j=0; j<16; j++){
    memmove(tk_table[i+1], tk1, size*sizeof(unsigned char));
    memmove(tk_table[i+1+37], tk2, size*sizeof(unsigned char));
    }
  }
}
