#include <stdio.h>
#include <math.h>

#include "add_round_tweakey.h"
#include "utility/print_utility.h"

void add_round_tweakey(unsigned char table[], unsigned char tk1[], unsigned char tk2[], int round, int size, char* mode){

  int square_root = sqrt(size);

  for(int i=0; i<(square_root/2); i++){
    for(int j=0; j<square_root; j++){
      table[i*(square_root)+j] = table[i*(square_root)+j] ^ tk1[i*(square_root)+j] ^ tk2[i*(square_root)+j];
    }
  }
  
  print_round(table, round, size, "AK", mode);
}
