#include <stdio.h>
#include <string.h>

#include "subcells.h"
#include "utility/print_utility.h"

const char SBOX[] = {0xc, 0x6, 0x9, 0x0, 0x1, 0xa, 0x2, 0xb, 0x3, 0x8, 0x5, 0xd, 0x4, 0xe, 0x7, 0xf};
const char SBOX_INV[] = {0x3, 0x4, 0x6, 0x8, 0xc, 0xa, 0x1, 0xe, 0x9, 0x2, 0x5, 0x7, 0x0, 0xb, 0xd, 0xf};

void subcells(unsigned char table[], int round, int size, char *mode){
  for(int i=0; i<size; i++){
    if(strcmp(mode, "DEC") == 0){
      table[i] = SBOX_INV[table[i]];
    }
    else if(strcmp(mode, "ENC") == 0){
      table[i] = SBOX[table[i]]; 
    }
  }

  print_round(table, round, size, "SC", mode);
}
