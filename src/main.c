#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "subcells.h"
#include "add_constants.h"
#include "add_round_tweakey.h"
#include "tweakey.h"
#include "shift_rows.h"
#include "mix_columns.h"
#include "utility/table_utility.h"
#include "utility/byte_utility.h"
#include "utility/print_utility.h"
#include "utility/timing.h"

const int STATE_LENGTH = 16;

void encryption(unsigned char text[], unsigned char tk1[], unsigned char tk2[], unsigned char **table){
  
  char* mode = "ENC";

  memmove(tk1, table[0], STATE_LENGTH*sizeof(unsigned char));
  memmove(tk2, table[37], STATE_LENGTH*sizeof(unsigned char));
  
  print_init(text, STATE_LENGTH, mode);
  print_tweakeys(tk1, tk2, STATE_LENGTH);
  printf("--------------------------------------\n");
  printf("Encryption start\n");
  printf("--------------------------------------\n");
  
  for(int round=0; round<36; round++){
    subcells(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);
    
    add_constants(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);

    add_round_tweakey(text, tk1, tk2, round, STATE_LENGTH, mode);
    memmove(tk1, table[round+1], STATE_LENGTH*sizeof(unsigned char));
    memmove(tk2, table[round+1+37], STATE_LENGTH*sizeof(unsigned char));
    print_tweakeys(tk1, tk2, STATE_LENGTH);

    shift_rows(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);

    mix_columns(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);
    printf("\n");
  }
  
  print_final(text, STATE_LENGTH, mode);
  print_tweakeys(tk1, tk2, STATE_LENGTH);
  printf("\n");
  
}

void decryption(unsigned char text[], unsigned char tk1[], unsigned char tk2[], unsigned char **table){
  
  char *mode = "DEC";

  memmove(tk1, table[36], STATE_LENGTH*sizeof(unsigned char));
  memmove(tk2, table[73], STATE_LENGTH*sizeof(unsigned char));
  
  print_init(text, STATE_LENGTH, mode);
  print_tweakeys(tk1, tk2, STATE_LENGTH);
  printf("--------------------------------------\n");
  printf("Decryption start\n");
  printf("--------------------------------------\n");
  
  for(int round=0; round<36; round++){
    
    mix_columns(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);

    shift_rows(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);

    memmove(tk1, table[73-(round+1+37)], STATE_LENGTH*sizeof(unsigned char));
    memmove(tk2, table[73-(round+1)], STATE_LENGTH*sizeof(unsigned char));
    add_round_tweakey(text, tk1, tk2, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);

    add_constants(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);

    subcells(text, round, STATE_LENGTH, mode);
    print_tweakeys(tk1, tk2, STATE_LENGTH);
    printf("\n");
  }

  print_final(text, STATE_LENGTH, mode);
  print_tweakeys(tk1, tk2, STATE_LENGTH);
  printf("\n");
  
}

int main(int argc, char *argv[]){
  if((argc == 3) | (argc == 4)){
    printf("P = %s,\nTK = %s\n", argv[1], argv[2]);

    unsigned char *text = (unsigned char*)malloc(STATE_LENGTH*sizeof(unsigned char));
    unsigned char *tk1 = (unsigned char*)malloc(STATE_LENGTH*sizeof(unsigned char));
    unsigned char *tk2 = (unsigned char*)malloc(STATE_LENGTH*sizeof(unsigned char));
    unsigned char **table = (unsigned char **) malloc(74*sizeof(unsigned char*));

    unsigned long long timer;
    
    for(int i=0; i<74; i++){
      table[i] = (unsigned char*) malloc(16*sizeof(unsigned char));
    }
    
    for(int i=0; i<STATE_LENGTH; i++){
      text[i] = ASCII_to_hex(argv[1][i]);
      tk1[i] = ASCII_to_hex(argv[2][i]);
      tk2[i] = ASCII_to_hex(argv[2][i+STATE_LENGTH]);
    }

    tweakey_calcul(table, tk1, tk2, STATE_LENGTH);

    if(argv[3]){
      if(strcmp(argv[3], "DEC") == 0){
	decryption(text, tk1, tk2, table);
      }
      else if(strcmp(argv[3], "ENC") == 0){
	timer = start_rdtsc();
	encryption(text, tk1, tk2, table);
	timer = end_rdtsc()- timer;
	printf("Cycles/byte: %f\n", (((double)timer)/8));
      }
    }
    else{
      encryption(text, tk1, tk2, table);
      decryption(text, tk1, tk2, table);
    }
    
    for(int i=0; i<74; i++){
      free(table[i]);
    }
    free(table);
    free(text);
    free(tk1);
    free(tk2);
  }
  else{
    printf("Wrong number of argument. Retry with at least a plain text and a key.");
  }
  
  return 0;
}
