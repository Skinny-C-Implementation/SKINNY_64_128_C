#include <stdio.h>
#include <string.h>

#include "utility/table_utility.h"
#include "shift_rows.h"
#include "utility/print_utility.h"


void shift_rows(unsigned char table[], int round, int size, char* mode){
  int permutation_table[16] = {0, 1, 2, 3, 7, 4, 5, 6, 10, 11, 8, 9, 13, 14, 15, 12};

  if(strcmp(mode, "ENC") == 0){
    table_permutation(table, permutation_table, size, 0);
  }
  else if(strcmp(mode, "DEC") == 0){
    table_permutation(table, permutation_table, size, 1);
  }

  print_round(table, round, size, "SR", mode);
}
