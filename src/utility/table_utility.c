#include "utility/table_utility.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void matrix_multiplication(char matrix_A[], char matrix_B[], char matrix_C[], int row, int col, int is_binary){
  for(int i=0; i<row; i++){
    for(int j=0; j<col; j++){
      matrix_C[i*row+j] = 0;
      for(int k=0; k<col; k++){
	matrix_C[i*row+j] += matrix_A[i*row+k]*matrix_B[k*col+j];
      }
      if(is_binary){
	matrix_C[i*row+j] = abs(matrix_C[i*row+j])%2;
	//printf("(%d)",matrix_C[i*row+j]);
      }
    }
    //printf("\n");
  }
}

void table_permutation(unsigned char table[], int index_table[], int size, char is_inv){
  unsigned char *tmp = (unsigned char*) malloc(size*sizeof(unsigned char));
  memmove(tmp, table, size*sizeof(unsigned char));
  for (int i=0; i<size; i++){
    if(!is_inv){
      table[i] = tmp[index_table[i]];
    }
    else{
      table[index_table[i]] = tmp[i];
    }
  }  
  free(tmp);
}
