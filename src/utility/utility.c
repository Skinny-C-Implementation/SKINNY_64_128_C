#include "utility/utility.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

void matrix_multiplication(unsigned char matrix_A[], unsigned char matrix_B[], unsigned char matrix_C[], int row, int col, int is_binary){
  for(int i=0; i<row; i++){
    for(int j=0; j<col; j++){
      matrix_C[i*row+j] = 0;
      for(int k=0; k<col; k++){
	matrix_C[i*row+j] += matrix_A[i*row+k]*matrix_B[k*col+j];
	if(is_binary){
	  matrix_C[i*row+j] %= 2;
	}
      }
    }
  }
}

void table_permutation(unsigned char table[], int index_table[], int size){
  unsigned char *tmp = (unsigned char*) malloc(size*sizeof(unsigned char));
  memmove(tmp, table, size*sizeof(unsigned char));
  for (int i=0; i<size; i++){
    table[i] = tmp[index_table[i]];
  }  
  free(tmp);
}

void byte_to_bit_table(unsigned char* byte, unsigned char table[], int size){
  for(int j=0; j<size; j++){
    table[size-(j+1)] = *byte & (0x01 << j);
    if(table[size-(j+1)]>0){
      table[size-(j+1)] = 1;
    }
    else{
      table[size-(j+1)] = 0;
    }
  }
}

void bit_table_to_byte(unsigned char* byte, unsigned char table[], int size){
  *byte = 0;
  for(int i=0; i<size; i++){
    *byte |= table[size-(i+1)] * (0x01 << i);
  }
}
