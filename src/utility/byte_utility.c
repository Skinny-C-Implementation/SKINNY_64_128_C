#include "utility/byte_utility.h"

void byte_to_bit_table(unsigned char* byte, unsigned char table[], int size){
  for(int j=0; j<size; j++){
    table[size-(j+1)] = *byte & (0x01 << j);
    if(table[size-(j+1)]>0){
      table[size-(j+1)] = 1;
    }
    else{
      table[size-(j+1)] = 0;
    }
  }
}

void bit_table_to_byte(unsigned char* byte, unsigned char table[], int size){
  *byte = 0;
  for(int i=0; i<size; i++){
    *byte |= table[size-(i+1)] * (0x01 << i);
  }
}

unsigned char ASCII_to_hex(char c){
  if((c >= 'A') & (c <= 'F')){
    return (unsigned char)(c - 'A' + 10);
  }
  else if((c >= 'a') & (c <= 'f')){
    return (unsigned char)(c - 'a' + 10);
  }
  else{
    return (unsigned char)(c - '0');
  }
}
