#include "utility/timing.h"

unsigned long long int start_rdtsc(void){
  unsigned a, d;

  __asm__ volatile("CPUID\n\t"
		   "RDTSC\n\t"
		   "mov %%edx, %0\n\t"
		   "mov %%eax, %1\n\t": "=r" (d),
		   "=r" (a):: "%rax", "%rbx", "%rcx", "%rdx");

  return ((unsigned long long)a) | (((unsigned long long)d) << 32);;
}

unsigned long long int end_rdtsc(void){
  unsigned a, d;

  __asm__ volatile("CPUID\n\t"
		   "RDTSC\n\t"
		   "mov %%edx, %0\n\t"
		   "mov %%eax,%1\n\t"
		   "CPUID\n\t": "=r" (d), "=r" (a)::
		   "%rax", "%rbx", "%rcx", "%rdx");

  return ((unsigned long long)a) | (((unsigned long long)d) << 32);;
}
