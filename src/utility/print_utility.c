#include "utility/print_utility.h"

#include <stdio.h>

void print_round(unsigned char table[], int round, int size, char* stage_str, char* mode){
  printf("%s - round %02d - after %s: S= ", mode, round, stage_str);
  
  for(int i=0; i<size; i++){
    printf("%x", table[i]);
  }
}

void print_init(unsigned char table[], int size, char* mode){
  printf("%s - Initial State - : S= ", mode);
  
  for(int i=0; i<size; i++){
    printf("%x", table[i]);
  }
}

void print_final(unsigned char table[], int size, char* mode){
  printf("--------------------------------------\n");
  printf("FINISH\n");
  printf("--------------------------------------\n");
  printf("%s - Final State - : S= ", mode);
  
  for(int i=0; i<size; i++){
    printf("%x", table[i]);
  }
}
