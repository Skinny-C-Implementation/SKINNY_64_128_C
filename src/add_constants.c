#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "add_constants.h"
#include "utility/print_utility.h"

const char LFSR_CONSTANTS[36] = {0x1, 0x3, 0x7, 0xf, 0x1f, 0x3e, 0x3d, 0x3b, 0x37, 0x2f, 0x1e, 0x3c, 0x39, 0x33, 0x27, 0x0e, 0x1d, 0x3a, 0x35, 0x2b, 0x16, 0x2c, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0b, 0x17, 0x2e, 0x1c, 0x38, 0x31, 0x23, 0x06, 0x0d};

void add_constants(unsigned char table[], int round, int size, char *mode){

  int m = round;
  if(strcmp(mode, "DEC") == 0){
    m = 36-(round+1);
  }
  
  char lfsr_table[16] = {0};
  lfsr_table[0] = LFSR_CONSTANTS[m] & 0x0f;
  lfsr_table[4] = (LFSR_CONSTANTS[m] >> 4) & 0x0f;
  lfsr_table[8] = 0x2;

  for(int i=0; i<size; i++){
    table[i] = table[i] ^ lfsr_table[i];
  }
  
  print_round(table, round, size, "AC", mode);
}
