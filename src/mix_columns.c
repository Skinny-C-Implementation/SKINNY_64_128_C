#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "utility/table_utility.h"
#include "utility/byte_utility.h"
#include "mix_columns.h"
#include "utility/print_utility.h"

void mix_columns_body(unsigned char table[], char binary_table[], int round, int size){
  int nib_size = sqrt(size);
  unsigned char *nib = (unsigned char*) malloc(nib_size*sizeof(unsigned char));
  unsigned char *nib_table = (unsigned char*) malloc(size*sizeof(unsigned char));
  char *tmp = (char*) malloc(size*sizeof(unsigned char));
  
  for(int i=0; i<nib_size; i++){
    //printf("\n");
    for(int j=0; j<nib_size; j++){
      byte_to_bit_table(&table[nib_size*j+i], nib, nib_size);
      /*
	for(int i = 0; i<4; i++){
	printf("%x|", nib[i]);
	}
	printf("\n");
      */
      memmove(nib_table+j*nib_size, nib, nib_size*sizeof(unsigned char));
    }
    /*for(int i = 0; i<16; i++){
      printf("%x|", nib_table[i]);
      }
      printf("\nMulti: ");*/
    matrix_multiplication(binary_table, (char*)nib_table, tmp, nib_size, nib_size, 1);
    /*for(int i = 0; i<16; i++){
      printf("%x|", tmp[i]);
      }
      printf("\n__");
    */
    
    for(int k=0; k<nib_size; k++){
      memmove(nib, tmp+k*nib_size, nib_size*sizeof(unsigned char));
      bit_table_to_byte(&table[nib_size*k+i], nib, nib_size);
    }
  }

  free(tmp);
  free(nib);
  free(nib_table);
}

void mix_columns(unsigned char table[], int round, int size, char* mode){
  char binary_table[16] = {1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0};
  char binary_table_inv[16] = {0, 1, 0, 0, 0, 1, 1, -1, 0, -1, 0, 1, 1, 0, 0, -1};
  //printf("comp: %d \n", strcmp(mode, "ENC"));
  if(strcmp(mode, "ENC") == 0){
    mix_columns_body(table, binary_table, round, size);
  }
  else if(strcmp(mode, "DEC") == 0){
    mix_columns_body(table, binary_table_inv, round, size);    
  }
  print_round(table, round, size, "MC", mode);
}
